package org.bdware.jwk;

import com.nimbusds.jose.jwk.JWK;
import com.nimbusds.jose.jwk.KeyUse;
import com.nimbusds.jose.jwk.RSAKey;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.zz.gmhelper.SM2Util;

import java.security.*;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.util.UUID;

public class KeyGenerator {
    static Logger LOGGER = LogManager.getLogger(KeyGenerator.class);

    @Test
    public void run() throws NoSuchAlgorithmException {
        KeyPairGenerator gen = KeyPairGenerator.getInstance("RSA");
        gen.initialize(2048);
        KeyPair keyPair = gen.generateKeyPair();
        JWK jwk = new RSAKey.Builder((RSAPublicKey) keyPair.getPublic())
                .privateKey((RSAPrivateKey) keyPair.getPrivate())
                .keyUse(KeyUse.ENCRYPTION)
                .keyID(UUID.randomUUID().toString())
                .build();
        LOGGER.info(jwk.toJSONString());
    }

    @Test
    public void genSM2() throws NoSuchAlgorithmException {
        LOGGER.info(SM2Util.generateSM2KeyPair().toJson());
    }

    @Test
    public void listProviders() {
        Provider[] providers = Security.getProviders();
        for (Provider p : providers) {
            System.out.println(p);
        }
    }
}
