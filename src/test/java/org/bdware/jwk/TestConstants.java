package org.bdware.jwk;

import com.nimbusds.jose.jwk.JWK;
import org.junit.Test;
import org.zz.gmhelper.SM2KeyPair;

import java.text.ParseException;

public class TestConstants {
    public static JWK serverKey = getServerKey();
    public static JWK clientKey = getClientKey();
    public static SM2KeyPair clientSM2 = SM2KeyPair.fromJson("{\"publicKey\":\"0404f1607dc48c7677f451387c4b4d1d9473cb984286f63d3a20530a731e9e25b4ac92d825df1afc11e38483e231c1730845304a961589d625ad4da3eee6061ee9\",\"privateKey\":\"56281c4fda9190e92363eeebb5dddda47b4ee0f15ea598af14704e3521ede9ee\"}");
    public static SM2KeyPair serverSM2 = SM2KeyPair.fromJson("{\"publicKey\":\"048387b21334cd8618a3821b6d57b1564af67861483d39a3a00ef8dc31c737c24f3c3a5f290bd2863c300a443428fae4a999a0a404e10d591a6cb3ab8a4041c062\",\"privateKey\":\"75e21a09547619934f4db238848ceda599ac619219018ac63663b78fd35daf2f\"}");

    private static JWK getServerKey() {
        String str = "{\"p\":\"_a6B9kW7DzBrs6P2HciO8OL6PThg-xe6vUyPgujdlP8gLFtr8FHkr5qg69-vN_hPbSloGDFlZKA4E9a00z1ki4bApH2KAaTUT6pUvxdbNv-JwOs2-2j8Ytbwdi8QhaziPJuSqafCYrVPCebUQE-ty2yBdjEMPMZ4AM1BjNBIKAU\",\"kty\":\"RSA\",\"q\":\"kTiiKZmzIn4d-iwzoKeV1hKehESoRZnXq0Me6aULoUNaF1Fz7KFicvP0Z_0fdnxJafUtLd_5z4TbWg8pzssz8936CHs2oe5U0cRGY3rJskrp0PuBfp67UIOPTBM6Ld9ZTYMpihrqJH8jBe13DlgoeZtU-9op8Xhi9p3cjCyZl3U\",\"d\":\"Sz5TPzXpmA-UQfQBEIU-P6ycqitYavWxn6RC_QMA2b4jYAYeUr6HK7eZ9JOcZEAyxOQKwhKbGZsTHu9IVgsnmAbs6Ypy_pQkSPzziVSszMetVJY1zvEVuEBlP1WHoH11mWiOoizoQ-kYlSIkL_qC47Kcc3yHdBPAGwehOYV8YOSl-XiX2IgsDIJpLJqLyGfbyFfOmpCUZbngaCl_msQQ9OiQXJ12fyIwTF1FOJwJ4e3mSPvo4WmeCsbsO6st-JRDaEFDOdlfD-KHQh65KDaZpkK3UaFjqkKJo16BgNaw1DeYxIBHLrrzRgOq2ee2Aa4dKKup94nDGqr6EYu_pWew8Q\",\"e\":\"AQAB\",\"use\":\"enc\",\"kid\":\"dcaf1347-8c74-483d-ac74-2c0bbb011741\",\"qi\":\"RhMzkITiYX87wfL5ODkOWL3Y6LaGK6gc222AlOF-GSYRKxoUvsCEpXGv4jKHBEeUXv6rxLuLa_wI2zNqL01RCkgRmYs3hx7CtAbHs4XIIYfZ3sMU-DAiJiF_T0DChDO00ySSUAJ6a_J71Ud0BMixVI7MFVj9ylgOxJDJol9WuF4\",\"dp\":\"Y1BDTt_DuNGTCJQDEWvoEgQ6RWdiCEsk72EeufhibydmOBdebYoSBnF52H4MwdOzfJ_-QaJs-HUFHzcqOZzKVRlfJ8aCFdyqxbmATgNd0W0_R8iOEOTsEeHl587LIBorw-CADW1A25XxqIW2yKqo9n-3O0c-bDii2GWC6RbNeOU\",\"dq\":\"Dhc3rN-sAQHJuNeHHuSD5mSiGuVqim5V_dkia7tG-JvHZxHRNLmoCs1e_qQR5HZEzVIr0xKzc45JlmB4RwdygAwe0ana8DVm53-q8MYeQf2A2HU-6GFQfYx2YARRldfhG2NJqYvZAjeP12hmL-8f5kTLJzDQ9wweVh2VI8jEEm0\",\"n\":\"j-f2epLdq_Z_5Ulc4z9V0sN7Zm5zreW450dId-Ag92xqlC-XWzOlBepFywOlPo6UTWr8ZHoNA_-G_SvuFILHt34qXNOin5VmIESV2a05ofoN2kyX0GsEEQyRzAoQE8LBaLvI8jK29vK6aWcVbzjhmmTmpwnJRhtjnP7nWQEY8pqQx2jdfRnDmRXT6BURWRf_A4yKbotVQWLC0rVJtI3_4y1czucDb8KyZTUE5VdD6jjnEZINhEh7KD5EQMEdA-Imzyt-trNc36aLBVumlf9NczbtP6Zxy3XHKkyEGtzXcat3UwuwRJdXVQKVFtcdNa1tBwHRYw7f1-ETWRTyh5I9SQ\"}";
        try {
            JWK jwk = JWK.parse(str);
            return jwk;
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void init() {
        System.out.println("Inited!");
    }

    private static JWK getClientKey() {
        String str = "{\"p\":\"5knPf4sOp5iQ5Z7NfWN3srqKUaasB69pIpn3X_bCiJ233Mkkye9kr5n7wxTZOU2Zmh_jdxF9OrBmIyWpoMptB3rC8A3c8NxF6dGAIkgB86krz26CeH7l_47so6cyYKuKaJUqXg2TzCWylzZ8fJ76Bx0yxA5B_1Sbz_Jnia6WeS8\",\"kty\":\"RSA\",\"q\":\"kcui1VGkfBD6m3DyY8hA68zfTl75HAiBuY_irct8qgtLg7dhxmRXqsSOkdZCHfSV0zalt8jS7dWl36_tFiXK2z-i13tP70EmDfJdh0OrA8QbAGRmXT8MUuKoUe8Lu5A5XqgHM6FwgfUWA5KYPAbLVi1sXLjXvPEKMXQf_K8qlJk\",\"d\":\"V4nLykoA63TciuCEP5JtjfCKvWprtZ8sELGnzR4Zfq2pjOjLHKzWZblJa7dacmgE2GRr7ZvIMBbrooJ8fuRnsM8BrTU4s6xz9z2887awUoWaO5sce9rCuio2SJtHpmw6vDI-oOQ42qy6qz_H7P6SyJz4T7qndhzFt3qZ9PcMPkpeacSSMLDGZs6GPkuSRL3CCw-kYaZFIkoL08Ivx8FrrV0YFv2L6egOnhrJJhW_gpQ3GkE4HyDsxIwp4JkPNOE02v-uL-_3pPrzenFLEG4Tf8FgGd6LvwBAE-5nH_fpxzMJmWqe0nmFcOtFjto3lPYjj9XBzkbtQb8DI-1qLxl6sQ\",\"e\":\"AQAB\",\"use\":\"enc\",\"kid\":\"8adbcbfe-199e-4a17-bfd1-821e86a1b47e\",\"qi\":\"LuWksKD__9o1LOyXkafJ6Tm5t7MT8SsDGb-GDT9gM7tOSJ6KZLKfDmtz2HEQNmMbjd-Z9N06pbIwkemE21iV6ps5rAaBGDbf83Npgvf-n3GKVR-k9Tgzmp48DQUAHUN7PfoAdKTm6JL0t8LpbGna394rMiNaZ6vgX8lJbClLaJU\",\"dp\":\"keG4HKfOhMTVJWDP89qK_SHGdasL12J7S3wVhSkgWsLusmKJd5K7SbJWFmKiqPZLk6MXyVm-5urQCPvW1RDmuJI_4yolCD_B2jjo3s6WzfAg3Kq44_QfZyD3L0S_WRXR_CZiTGp1ciF_XOMbQSbEZLVOb3xIuqKygayhgkqi7-k\",\"dq\":\"g448YEBLK6gmehxwm5kW-67h0NXh8mm6pLYw1KHI26dVfIT8tQfWE0FJZE7xWhZZGz00S6HqsrEV-8HDLTjs0umZBtc-SaV_sRYBTwzAQ6Wwt6ngtEMv25qqR0RsUdLR8Zes5-nEm3-LXa3psEBxOlHCdehUwyi9CeK--kMVKQ\",\"n\":\"gyb9iWIQPjWYkFNVIobtUMffutTzY25VMqyK1ZZbUCqnQ5FL-mej1Br8cRh7PNmKK798Q3DilFQtxhvu8Uxb9Z86ZunKLNj67JkBX0HAZFIf8wP_aKFIaq6yGp0vL5W6UwEEbNjmG8ja7NYNICNk0F28pLPxm5AmCrYxSTyMvtrdeg90nlq0aweQGF51Y717NvhR7gdbxpEjWbrkn0Y5J7Kv7MxRSz5pxg8xjsGBMtC6ZhxjxxseIfEauOfqPuThSKX2oijyMUzEBVK_Zw7nydG_AW8KAz0RpDmmq3-mQZw1kgrhZ7avR6Vqynv0JMt-slbNarap88aDwvcDWLOZFw\"}";
        try {
            JWK jwk = JWK.parse(str);
            return jwk;
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }
}
