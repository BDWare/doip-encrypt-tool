package org.bdware.jwk;

import com.nimbusds.jose.jwk.JWK;
import org.bdware.doip.codec.doipMessage.DoipMessage;
import org.bdware.doip.encrypt.KeyRetriever;

public class TestClientKeyRetriever implements KeyRetriever<JWK> {
    @Override
    public JWK retriveKey(DoipMessage message) {
        return TestConstants.serverKey.toPublicJWK();
    }
}
