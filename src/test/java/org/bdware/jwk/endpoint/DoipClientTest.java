/*
 *    Copyright (c) [2021] [Peking University]
 *    [BDWare DOIP SDK] is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *             http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

package org.bdware.jwk.endpoint;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bdware.doip.codec.digitalObject.DigitalObject;
import org.bdware.doip.codec.doipMessage.DoipMessage;
import org.bdware.doip.codec.exception.DoDecodeException;
import org.bdware.doip.encrypt.JWKCryptoManager;
import org.bdware.doip.encrypt.SM2Signer;
import org.bdware.doip.endpoint.client.ClientConfig;
import org.bdware.doip.endpoint.client.DoipClientImpl;
import org.bdware.doip.endpoint.client.DoipMessageCallback;
import org.bdware.jwk.TestClientKeyRetriever;
import org.bdware.jwk.TestConstants;
import org.junit.Test;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.atomic.AtomicInteger;

public class DoipClientTest {
    static Logger LOGGER = LogManager.getLogger(DoipClientTest.class);
    int totalCount = 1;
    String serviceAddress = "tcp://127.0.0.1:21042";
    @Test
    public void testRetrieveReconnect() throws InterruptedException {
        long start = System.currentTimeMillis();
        final AtomicInteger total = new AtomicInteger(0);
        final AtomicInteger correct = new AtomicInteger(0);
        for (int i = 0; i < totalCount; i++) {
            final DoipClientImpl doipClient = new DoipClientImpl();
            doipClient.connect(ClientConfig.fromUrl(serviceAddress)
                    .setDebugPrint(true)
                    .setSigner(new SM2Signer(TestConstants.clientSM2))
                    .setCryptoManager(new JWKCryptoManager(TestConstants.clientKey, new TestClientKeyRetriever())));
            doipClient.retrieve("aibd/small", null, true, new DoipMessageCallback() {
                @Override
                public void onResult(DoipMessage msg) {
                    try {
                        DigitalObject digitalObject = msg.body.getDataAsDigitalObject();
                        byte[] data = digitalObject.elements.get(0).getData();
                        if (new String(data, StandardCharsets.UTF_8).contains("cold"))
                            correct.incrementAndGet();
                    } catch (DoDecodeException e) {
                        throw new RuntimeException(e);
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                    total.incrementAndGet();
                    if (doipClient != null) doipClient.close();
                }
            });
        }
        int circle = 0;
        for (; total.get() < totalCount; ) {
            if (++circle % 100 == 0)
                LOGGER.info(String.format("%d/%d", correct.get(), total.get()));
            Thread.sleep(10);
        }
        int dur = (int) (System.currentTimeMillis() - start);
        LOGGER.info(String.format("Final Result:%d/%d dur:%d", correct.get(), total.get(), dur));
    }
}