/*
 *    Copyright (c) [2021] [Peking University]
 *    [BDWare DOIP SDK] is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *             http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

package org.bdware.jwk.endpoint;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bdware.doip.encrypt.JWKCryptoManager;
import org.bdware.doip.encrypt.SM2Signer;
import org.bdware.doip.endpoint.server.DoipListenerConfig;
import org.bdware.doip.endpoint.server.DoipServerImpl;
import org.bdware.doip.endpoint.server.DoipServiceInfo;
import org.bdware.jwk.TestConstants;
import org.bdware.jwk.TestServerKeyRetriever;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class TestServer {
    static Logger LOGGER = LogManager.getLogger(TestServer.class);

    public static void main(String[] arg) throws InterruptedException {
        int port = 21042;
        if (arg != null && arg.length > 0) {
            port = Integer.valueOf(arg[0]);
        }
        run(port);
    }

    public static void run(int port) throws InterruptedException {
        List<DoipListenerConfig> infos = new ArrayList<>();
        try {
            infos.add(new DoipListenerConfig("tcp://127.0.0.1:" + port, "2.1")
                    .setDebugPrint(true)
                    .setSigner(new SM2Signer(TestConstants.serverSM2))
                    .setCryptoManager(new JWKCryptoManager(TestConstants.serverKey,
                            new TestServerKeyRetriever())));
        } catch (Exception e) {
            e.printStackTrace();
        }
        DoipServiceInfo info = new DoipServiceInfo("bdware.test/do.12345", "ownerDEF", "gateRepo", infos);
        DoipServerImpl server = new DoipServerImpl(info);
        final AtomicInteger count = new AtomicInteger(0);
        TestRepoHandler handler = new TestRepoHandler();
        handler.count = count;
        server.setRepositoryHandler(handler);
        server.start();
        for (; ; ) {
            LOGGER.info("Count:" + count.get());
            Thread.sleep(10000);
        }
    }
}
