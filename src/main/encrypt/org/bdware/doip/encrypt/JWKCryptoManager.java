package org.bdware.doip.encrypt;

import com.nimbusds.jose.*;
import com.nimbusds.jose.crypto.DirectDecrypter;
import com.nimbusds.jose.crypto.DirectEncrypter;
import com.nimbusds.jose.crypto.RSADecrypter;
import com.nimbusds.jose.crypto.RSAEncrypter;
import com.nimbusds.jose.jwk.JWK;
import org.bdware.doip.codec.doipMessage.DoipMessage;
import org.bdware.doip.codec.doipMessage.MessageCredential;
import org.bdware.doip.endpoint.CryptoManager;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;

//TODO publickey should retrieve?
public class JWKCryptoManager implements CryptoManager<JWK, SecretKey> {
    SecretKey aes;
    JWK ownKeyPair;
    KeyRetriever<JWK> retriever;

    public JWKCryptoManager(JWK ownKeyPair, KeyRetriever<JWK> retriever) {
        this.ownKeyPair = ownKeyPair;
        this.retriever = retriever;
    }

    @Override
    public SecretKey getSymmetricKey(DoipMessage message) {
        if (aes == null) aes = generateOne();
        return aes;
    }

    private SecretKey generateOne() {
        try {
            int keyBitLength = EncryptionMethod.A256GCM.cekBitLength();
            KeyGenerator keyGen = KeyGenerator.getInstance("AES");
            keyGen.init(keyBitLength);
            SecretKey key = keyGen.generateKey();
            return key;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public JWK getPublicKey(DoipMessage message) {
        return retriever.retriveKey(message);
    }

    @Override
    public JWK getOwnKeyPair(DoipMessage message) {
        return ownKeyPair;
    }

    @Override
    public byte[] encryptUseSymmetricKey(byte[] encodedData, SecretKey symmetricKey) {
        try {
            JWEHeader header = new JWEHeader(JWEAlgorithm.DIR, EncryptionMethod.A256GCM);
            Payload payload = new Payload(encodedData);
            JWEObject jweObject = new JWEObject(header, payload);
            jweObject.encrypt(new DirectEncrypter(symmetricKey));
            String jweString = jweObject.serialize();
            return jweString.getBytes(StandardCharsets.UTF_8);
        } catch (Throwable t) {
            t.printStackTrace();
        }
        return encodedData;
    }

    @Override
    public String encryptSymmetricKey(SecretKey symmetricKey, JWK publicKey) {
        JWEHeader header = new JWEHeader(
                JWEAlgorithm.RSA_OAEP_256,
                EncryptionMethod.A128GCM
        );
        byte[] key = symmetricKey.getEncoded();
        JWEObject object = new JWEObject(header, new Payload(key));
        try {
            RSAEncrypter encrypter = new RSAEncrypter(publicKey.toRSAKey().toRSAPublicKey());
            object.encrypt(encrypter);
        } catch (JOSEException e) {
            throw new RuntimeException(e);
        }
        return object.serialize();
    }

    @Override
    public SecretKey decryptSymmetricKey(String encryptedSymmetricKey, JWK mykey) {
        try {
            JWEObject object = JWEObject.parse(encryptedSymmetricKey);
            object.decrypt(new RSADecrypter(mykey.toRSAKey()));
            byte[] bytes = object.getPayload().toBytes();
            SecretKey key = new SecretKeySpec(bytes, "AES");
            return key;
        } catch (JOSEException e) {
            throw new RuntimeException(e);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public byte[] decryptUseSymmetricKey(byte[] encodedData, SecretKey symmetricKey) {
        JWEObject jweObject = null;
        try {
            jweObject = JWEObject.parse(new String(encodedData, StandardCharsets.UTF_8));
            jweObject.decrypt(new DirectDecrypter(symmetricKey));
            Payload payload = jweObject.getPayload();
            return payload.toBytes();
        } catch (ParseException e) {
            throw new RuntimeException(e);
        } catch (KeyLengthException e) {
            throw new RuntimeException(e);
        } catch (JOSEException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void putPubKeyToCredential(DoipMessage message, JWK publicKey) {
        if (message.credential == null) message.credential = new MessageCredential((String) null);
        message.credential.setAttributes("clientPublicKey", publicKey.toPublicJWK().toJSONString());
    }

    @Override
    public JWK getPubKeyFromCredential(DoipMessage message) {
        try {
            String str = message.credential.getAttriburte("clientPublicKey").getAsString();
            return JWK.parse(str);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }
}
