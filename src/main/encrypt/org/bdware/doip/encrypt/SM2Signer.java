package org.bdware.doip.encrypt;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bdware.doip.codec.doipMessage.DoipMessage;
import org.bdware.doip.codec.doipMessage.DoipMessageSigner;
import org.bdware.doip.codec.doipMessage.MessageCredential;
import org.bdware.irp.irplib.core.IrpMessage;
import org.bdware.irp.irplib.core.IrpMessageSigner;
import org.bdware.irp.irplib.util.EncoderUtils;
import org.bouncycastle.crypto.params.ECPublicKeyParameters;
import org.bouncycastle.pqc.math.linearalgebra.ByteUtils;
import org.zz.gmhelper.BCECUtil;
import org.zz.gmhelper.SM2KeyPair;
import org.zz.gmhelper.SM2Util;

import java.nio.charset.StandardCharsets;

//This class is lower than IrpHandler
//Only resolve operation can be anonymous
public class SM2Signer implements IrpMessageSigner, DoipMessageSigner {
    static Logger LOGGER = LogManager.getLogger(SM2Signer.class);
    SM2KeyPair keyPair;
    boolean enableSign;
    boolean enableVerify;

    public SM2Signer(SM2KeyPair keyPair) {
        this(keyPair, keyPair != null, keyPair != null);
    }

    public SM2Signer(SM2KeyPair keyPair, boolean enableSign, boolean enableVerify) {
        this.keyPair = keyPair;
        this.enableSign = enableSign;
        this.enableVerify = enableVerify;
    }

    public SM2KeyPair getKeyPair() {
        return keyPair;
    }

    @Override
    public void signMessage(IrpMessage msg) {
        if (!enableSign) return;
        try {
            //TODO sign here
            msg.header.setCertifiedFlag(true);
            byte[] digestData = msg.getEncodedMessageHeaderBody();
            byte[] signature = SM2Util.sign(keyPair.getPrivateKeyParameter(), digestData);
            msg.credential.signerDoid = keyPair.getPublicKeyStr().getBytes(StandardCharsets.UTF_8);
            msg.credential.signedInfoType = EncryptConstants.CREDENTIAL_SIGNEDINFO_TYPE_SM2;
            msg.credential.signedInfoDigestAlgorithm = EncryptConstants.CREDENTIAL_DIGEST_ALG_SM2;
            msg.credential.signature = signature;
            msg.credential.signedInfoLength = msg.credential.signedInfoDigestAlgorithm.length + msg.credential.signature.length;
            msg.encodedMessage = null;  //update the encode message
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public boolean verifyMessage(IrpMessage msg) {
        if (!enableVerify) return true;
        try {
            byte[] digestData = msg.getEncodedMessageHeaderBody();
            byte[] signature = msg.credential.signature;
            String pubkeyStr = EncoderUtils.decodeString(msg.credential.signerDoid);
            ECPublicKeyParameters pubkey = BCECUtil.createECPublicKeyFromStrParameters(
                    pubkeyStr, SM2Util.CURVE, SM2Util.DOMAIN_PARAMS);
            boolean result = SM2Util.verify(pubkey, digestData, signature);
            //   LOGGER.debug("verify!" + result);
            return result;
        } catch (Exception e) {
            LOGGER.error("verify! failed");
            LOGGER.debug(e);
        }
        return false;
    }

    @Override
    public void signMessage(DoipMessage msg) {
        if (!enableSign) return;
        try {
            msg.header.setIsCertified(true);
            byte[] digestData = msg.getDoipMessageHeaderBody();
            byte[] signature = SM2Util.sign(keyPair.getPrivateKeyParameter(), digestData);
            if (msg.credential == null) msg.credential = new MessageCredential((String) null);
            msg.credential.setSigner(keyPair.getPublicKeyStr());
            msg.credential.setSignature(signature);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String prettyBytes(byte[] digestData) {
        return ByteUtils.toHexString(digestData);
    }


    @Override
    public boolean verifyMessage(DoipMessage msg) {
        if (!enableVerify) return true;
        try {
            String pubkeyStr = msg.credential.getSigner();
            byte[] digestData = msg.getDoipMessageHeaderBody();
            byte[] signature = msg.credential.getSignature();
            ECPublicKeyParameters pubkey = BCECUtil.createECPublicKeyFromStrParameters(
                    pubkeyStr, SM2Util.CURVE, SM2Util.DOMAIN_PARAMS);
            boolean result = SM2Util.verify(pubkey, digestData, signature);
            return result;
        } catch (Exception e) {
        }
        return false;
    }

}
