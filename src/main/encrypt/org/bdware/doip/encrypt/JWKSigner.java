package org.bdware.doip.encrypt;

import com.nimbusds.jose.jwk.JWK;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bdware.doip.codec.doipMessage.DoipMessage;
import org.bdware.doip.codec.doipMessage.DoipMessageSigner;
import org.bdware.doip.codec.doipMessage.MessageCredential;
import org.bdware.irp.deprecated.GlobalUtils;
import org.bdware.irp.irplib.core.IrpMessage;
import org.bdware.irp.irplib.core.IrpMessageSigner;
import org.bdware.irp.irplib.util.EncoderUtils;
import org.bdware.irp.irplib.util.IrpCommon;

import java.security.PublicKey;

public class JWKSigner implements DoipMessageSigner, IrpMessageSigner {
    private static final Logger LOGGER = LogManager.getLogger(JWKSigner.class);

    public static class SessionKey {
        private boolean enable = false;

        private String key = null;

        private boolean isSendingAuthorized = false;
        private boolean isRecivingAuthorized = false;
    }

    private final String doid;
    private final JWK jwk;

    private final SessionKey session = new SessionKey();

    public JWKSigner(JWK jwk, String doid) {
        this.jwk = jwk;
        this.doid = doid;
        this.session.enable = true;
    }

    public synchronized void enableSessionToken() {
        this.session.enable = true;
    }

    public synchronized void disableSessionToken() {
        this.session.enable = false;
    }

    public synchronized void reset() {
        this.session.isSendingAuthorized = false;
        this.session.isRecivingAuthorized = false;
        this.session.key = null;
    }

    //sign the massage by Signature, need init first
    @Override
    public synchronized final boolean verifyMessage(DoipMessage doipMessage) {
        try {

            return verifyMessage(doipMessage, jwk.toRSAKey().toPublicKey());
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public synchronized void signMessage(DoipMessage doipMessage) {
        if (this.session.enable) {
            //System.out.println("use doip token while sending");
            signMessageWithToken(doipMessage);
        } else {
            signMessageWithSign(doipMessage);
        }
    }

    private void signMessageWithSign(DoipMessage doipMessage) {
        try {
            if (jwk == null) return;
            doipMessage.header.setIsCertified(true);
            //generate the message signature data first
            byte[] digestData = doipMessage.getDoipMessageHeaderBody();
            //not digest the body and header, sign immediately
            String signature = GlobalUtils.signByteArrayByJWK(digestData, jwk);
            if (doipMessage.credential == null) {
                doipMessage.credential = new MessageCredential(doid);
            }
            doipMessage.credential.setSigner(doid);
            doipMessage.credential.setSignature(EncoderUtils.encodeString(signature));
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
    }

    private void signMessageWithToken(DoipMessage doipMessage) {
        try {
            if (jwk == null) return;
            doipMessage.header.setIsCertified(true);
            //generate the message signature data first
            String signature = this.session.key;
            if (!this.session.isSendingAuthorized) {
                //byte[] digestData = doipMessage.getDoipMessageHeaderBody();
                byte[] digestData = (this.doid).getBytes();
                //not digest the body and header, sign immediately
                signature = GlobalUtils.signByteArrayByJWK(digestData, jwk);
            }

            if (doipMessage.credential == null) {
                doipMessage.credential = new MessageCredential(doid);
            }
            doipMessage.credential.setSigner(doid);
            doipMessage.credential.setSignature(EncoderUtils.encodeString(signature));

            if (!this.session.isSendingAuthorized) {
                this.session.key = signature;
                this.session.isSendingAuthorized = true;
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
    }

    public synchronized boolean verifyMessage(DoipMessage doipMessage, PublicKey publicKey) {
        if (this.session.enable) {
            //System.out.println("use doip token while recieving");
            return verifyMessageWithToken(doipMessage, publicKey);
        } else {
            return verifyMessageWithSign(doipMessage, publicKey);
        }
    }


    private boolean verifyMessageWithSign(DoipMessage doipMessage, String pubKeyStr) {
        try {
            byte[] digestData = doipMessage.getDoipMessageHeaderBody();
            String signature = EncoderUtils.decodeString(doipMessage.credential.getSignature());
            return GlobalUtils.verifySigByJWK(digestData, signature, pubKeyStr);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        return false;
    }

    private boolean verifyMessageWithSign(DoipMessage doipMessage, PublicKey publicKey) {
        String pubKeyStr = GlobalUtils.createPublicKeyString(publicKey);
        if (pubKeyStr == null) return false;

        return verifyMessageWithSign(doipMessage, pubKeyStr);
    }

    private boolean verifyMessageWithToken(DoipMessage doipMessage, PublicKey publicKey) {
        String signature = EncoderUtils.decodeString(doipMessage.credential.getSignature());
        if (this.session.isRecivingAuthorized) {
            return true;
        }

        String pubKeyStr = GlobalUtils.createPublicKeyString(publicKey);
        if (pubKeyStr == null) return false;

        try {
            //byte[] digestData = doipMessage.getDoipMessageHeaderBody();
            byte[] digestData = (this.doid).getBytes();
            //String signature = EncoderUtils.decodeString(doipMessage.credential.getSignature());
            boolean res = GlobalUtils.verifySigByJWK(digestData, signature, pubKeyStr);
            if (res) {
                this.session.isRecivingAuthorized = true;
            }
            return res;
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        return false;
    }

    public static final void signMessage(IrpMessage irpMessage, JWK jwk) {
        try {
            if (jwk == null) return;
            irpMessage.header.setCertifiedFlag(true);
            //generate the message signature data first
            byte[] digestData = irpMessage.getEncodedMessageHeaderBody();
            //not digest the body and header, sign immediately
            String signature = GlobalUtils.signByteArrayByJWK(digestData, jwk);
            irpMessage.credential.signerDoid = EncoderUtils.encodeString(jwk.getKeyID());
            irpMessage.credential.signedInfoType = IrpCommon.CREDENTIAL_SIGNEDINFO_TYPE_JWK;
            irpMessage.credential.signedInfoDigestAlgorithm = IrpCommon.CREDENTIAL_DIGEST_ALG_JWK;
            irpMessage.credential.signature = EncoderUtils.encodeString(signature);
            irpMessage.credential.signedInfoLength = irpMessage.credential.signedInfoDigestAlgorithm.length + irpMessage.credential.signature.length;
            irpMessage.encodedMessage = null;  //update the encode message
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void signMessage(IrpMessage irpMessage) {
        signMessage(irpMessage, jwk);
    }

    //sign the massage by Signature, need init first
    public final boolean verifyMessage(IrpMessage irpMessage) {
        try {
            //generate the message signature data first
            byte[] digestData = irpMessage.getEncodedMessageHeaderBody();
            String signature = EncoderUtils.decodeString(irpMessage.credential.signature);
            String pubkey = EncoderUtils.decodeString(irpMessage.credential.signerDoid);
            return GlobalUtils.verifySigByJWK(digestData, signature, pubkey);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}
