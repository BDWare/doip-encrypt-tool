package org.bdware.doip.encrypt;

import org.bdware.doip.codec.doipMessage.DoipMessage;

public interface KeyRetriever<T> {
    T retriveKey(DoipMessage message);
}
