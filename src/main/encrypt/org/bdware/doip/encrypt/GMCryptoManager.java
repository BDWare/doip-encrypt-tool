package org.bdware.doip.encrypt;

import org.bdware.doip.codec.doipMessage.DoipMessage;
import org.bdware.doip.codec.doipMessage.MessageCredential;
import org.bdware.doip.endpoint.CryptoManager;
import org.bouncycastle.crypto.params.ECPublicKeyParameters;
import org.bouncycastle.pqc.math.linearalgebra.ByteUtils;
import org.zz.gmhelper.BCECUtil;
import org.zz.gmhelper.SM2KeyPair;
import org.zz.gmhelper.SM2Util;
import org.zz.gmhelper.SM4Util;

public class GMCryptoManager implements CryptoManager<SM2KeyPair, byte[]> {
    byte[] sm4key;
    SM2KeyPair ownKeyPair;
    KeyRetriever<SM2KeyPair> retriever;

    public GMCryptoManager(SM2KeyPair ownKeyPair, KeyRetriever<SM2KeyPair> retriever) {
        this.ownKeyPair = ownKeyPair;
        this.retriever = retriever;
    }

    @Override
    public byte[] getSymmetricKey(DoipMessage message) {
        if (sm4key == null) sm4key = generateOne();
        return sm4key;
    }

    private byte[] generateOne() {
        try {
            return SM4Util.generateKey();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public SM2KeyPair getPublicKey(DoipMessage message) {
        return retriever.retriveKey(message);
    }

    @Override
    public SM2KeyPair getOwnKeyPair(DoipMessage message) {
        return ownKeyPair;
    }

    @Override
    public byte[] encryptUseSymmetricKey(byte[] encodedData, byte[] symmetricKey) {
        try {
            return SM4Util.encrypt_ECB_NoPadding(symmetricKey, encodedData);
        } catch (Throwable t) {
            t.printStackTrace();
        }
        return encodedData;
    }

    @Override
    public String encryptSymmetricKey(byte[] symmetricKey, SM2KeyPair publicKey) {
        try {
            return ByteUtils.toHexString(SM2Util.encrypt(publicKey.getPublicKey(), symmetricKey));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public byte[] decryptSymmetricKey(String encryptedSymmetricKey, SM2KeyPair mykey) {
        try {
            return (SM2Util.decrypt(mykey.getPrivateKeyParameter(), ByteUtils.fromHexString(encryptedSymmetricKey)));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public byte[] decryptUseSymmetricKey(byte[] encodedData, byte[] symmetricKey) {
        try {
            return SM4Util.decrypt_ECB_NoPadding(symmetricKey, encodedData);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void putPubKeyToCredential(DoipMessage message, SM2KeyPair publicKey) {
        if (message.credential == null) message.credential = new MessageCredential((String) null);
        message.credential.setAttributes("clientPublicKey", publicKey.getPublicKeyStr());
    }

    @Override
    public SM2KeyPair getPubKeyFromCredential(DoipMessage message) {
        try {
            String str = message.credential.getAttriburte("clientPublicKey").getAsString();
            ECPublicKeyParameters point =
                    BCECUtil.createECPublicKeyFromStrParameters(
                            str, SM2Util.CURVE, SM2Util.DOMAIN_PARAMS);
            SM2KeyPair pair = new SM2KeyPair(point, null);
            return pair;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
