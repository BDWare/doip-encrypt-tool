package org.bdware.doip.encrypt;


import org.bdware.irp.irplib.util.EncoderUtils;

public class EncryptConstants {
    public static final byte[] CREDENTIAL_SIGNEDINFO_TYPE_SM2 = EncoderUtils.encodeString("HS_SIGNED_SM2");
    public static final byte[] CREDENTIAL_DIGEST_ALG_SM2 = EncoderUtils.encodeString("JWK");

    public static final byte[] CREDENTIAL_SIGNEDINFO_TYPE_RSA = EncoderUtils.encodeString("HS_SIGNED_RSA");

}